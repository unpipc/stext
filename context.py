#!/usr/bin/env python

import unittest
import operator
from expression import *
import localization
import statements
from framed_data import FramedData
import helpers


class GlobalObjects(dict):
    def __getitem__(self, path):
        keys = path.split('.')
        item = super(GlobalObjects, self).__getitem__(keys[0])

        return helpers.deep_get(item, keys[1:])

    def __setitem__(self, key, value):
        if key in self:
            raise KeyError("Already assigned: {}".format(key))

        super(GlobalObjects, self).__setitem__(key, value)


class Context(object):
    def __init__(self, g_dict, locale, convert_to='unicode'):
        self.global_objects = GlobalObjects(g_dict)
        self.local_objects = FramedData()
        self.local_settings = FramedData()
        self.convert_to = convert_to

        self.local_settings.open()
        self.local_settings['locale'] = locale

        self.__framed_objects = (self.local_objects, self.local_settings)

    def __getitem__(self, key):
        keys = key.split('.')
        key0 = keys[0]

        try:
            # Check that parent object is among locals:
            _ = self.local_objects[key0]
        except KeyError:
            return helpers.deep_get(self.global_objects, keys)

        return self.local_objects[key]

    def open(self):
        for fo in self.__framed_objects:
            fo.open()

    def close(self):
        for fo in self.__framed_objects:
            fo.close()


class Functions(object):
    @staticmethod
    def bold(ctx, value):
        return UnicodeExpr(u"*{}*".format(value.to_unicode(ctx)))

    @staticmethod
    def italic(ctx, value):
        return UnicodeExpr(u"_{}_".format(value.to_unicode(ctx)))

    @staticmethod
    def dummy_locale(ctx, value):
        return value

    @staticmethod
    def format_number(ctx, value):
        return UnicodeExpr(value.to_unicode(ctx))

    @staticmethod
    def dummy_nice_heading(ctx, value):
        return UnicodeExpr("<Nice heading>")

    @staticmethod
    def dummy_func(ctx, value):
        return UnicodeExpr("<func()>")

    @staticmethod
    def capitalize(ctx, stext):
        return stext


class FBoldBodyExpr(BaseExpr):
    def calculate(self, ctx):
        return UnicodeExpr(self.to_unicode(ctx))

    def to_unicode(self, ctx):
        inner = NameExpr('arg').calculate(ctx)

        if ctx.convert_to == 'unicode':
            fmt = u'{}'

        if ctx.convert_to == 'html':
            fmt = u"<span style='font-weight: bold;'>{}</span>"

        return fmt.format(inner.value)


class FBolderBodyExpr(BaseExpr):
    def calculate(self, ctx):
        return UnicodeExpr(self.to_unicode(ctx))

    def to_unicode(self, ctx):
        inner = NameExpr('arg').calculate(ctx)

        if ctx.convert_to == 'unicode':
            fmt = u'{}'

        if ctx.convert_to == 'html':
            fmt = u"<span style='font-weight: bolder;'>{}</span>"

        return fmt.format(inner.value)


class FItalicBodyExpr(BaseExpr):
    def calculate(self, ctx):
        return UnicodeExpr(self.to_unicode(ctx))

    def to_unicode(self, ctx):
        inner = NameExpr('arg').calculate(ctx)

        if ctx.convert_to == 'unicode':
            fmt = u'{}'

        if ctx.convert_to == 'html':
            fmt = u"<span style='font-style: italic;'>{}</span>"

        return fmt.format(inner.value)


class FCapitalizeBodyExpr(BaseExpr):
    def calculate(self, ctx):
        return UnicodeExpr(self.to_unicode(ctx))

    def to_unicode(self, ctx):
        expr = NameExpr('arg').calculate(ctx)

        if not isinstance(expr, UnicodeExpr):
            raise SemanticError(index(expr), "Expected `{}`".format(UnicodeExpr.expr_name))

        return expr.value.capitalize()


class FLocaleFlagExpr(BaseExpr):
    def __init__(self, locale_obj, element=None):
        super(FLocaleFlagExpr, self).__init__(element)

        self.locale_obj = locale_obj

    def calculate(self, ctx):
        ctx.local_settings['locale'] = self.locale_obj

        return NoneExpr()

functions = {'bold': Function(['arg'], FBoldBodyExpr()),
             'bolder': Function(['arg'], FBolderBodyExpr()),
             'italic': Function(['arg'], FItalicBodyExpr()),
             'num': Function(['arg'], NameExpr('arg')),
             'nice_heading': Function(['arg'], NameExpr('arg')),
             'func': Function(['arg'], NameExpr('arg')),
             'capitalize': Function(['arg'], FCapitalizeBodyExpr())}


locales = {'ru': FLocaleFlagExpr(localization.ru),
           'ru2': FLocaleFlagExpr(localization.ru2),
           'fr': FLocaleFlagExpr(localization.fr),
           'uk': FLocaleFlagExpr(localization.uk)}

locales = {locale.name: FLocaleFlagExpr(locale) for locale in localization.all_locales}

#locales = {'ru': localization.ru,
#           'en': localization.en}

global_objects = functions
global_objects.update({'locale': locales})

class Test(unittest.TestCase):
    def test_all(self):
        ctx = Context({'A': 'a', 'B': 'b'})
        self.assertEqual(ctx['A'], 'a')
        self.assertRaises(KeyError, operator.setitem, ctx.global_objects, 'A', 'aa')

        ctx.local_objects.open()

        ctx.local_objects['A'] = 'aa'
        self.assertEqual(ctx['A'], 'aa')

        ctx.local_objects.close()

        self.assertEqual(ctx['A'], 'a')


if __name__ == '__main__':
    unittest.main()