# -*- coding: utf-8 -*-


class Locale(object):
    def __init__(self, name, quotes_map):
        self.name = name
        self.quotes_map = quotes_map


ru = Locale('ru',
            {'<<': u'«',
             '>>': u'»',
             '``': u'„',
             "''": u'“',
             '"': u'“',
             "'": u'’'})

ru2 = Locale('ru2',
             {'``': u'„',
              "''": u'”',
              '"': u'”',
              "'": u'’'})

uk = Locale('uk',
            {'`': u'‘',
             "'": u'’',
             '``': u'“',
             "''": u'”',
             '"': u'”'})

fr = Locale('fr',
            {'<<': u'«',
             '>>': u'»',
             '``': u'“',
             "''": u'”',
             '"': u'”',
             "'": u'’'})

ch = Locale('ch',
            {'<<': u'«',
             '>>': u'»',
             '<': u'‹',
             ">": u'›',
             "'": u'’'})

de = Locale('de',
             {'<<': u'„',
              '>>': u'“',
              '<': u'‚',
              ">": u'‘',
              "'": u'’'})

de2 = Locale('de2',
             {'<<': u'«',
              '>>': u'»',
              '<': u'‹',
              ">": u'›',
              "'": u'’'})

all_locales = {ru, ru2, uk, fr, ch, de, de2}