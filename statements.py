class Definition(object):
    def __init__(self, statements):
        self.statements = statements

    def execute(self, ctx):
        for statement in self.statements:
            statement.execute(ctx)


class Assignment(object):
    def __init__(self, name, expr):
        self.name = name
        self.expr = expr

    def execute(self, ctx):
        ctx.local_objects[self.name] = self.expr
