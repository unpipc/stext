import abc
from assertions import *


class BaseType(object):
    __metaclass__ = abc.ABCMeta

    def to_unicode(self, ctx):
        pass


class Number(BaseType):
    def __init__(self, value):
        self.value = value
        self.check()

    def check(self):
        assert number(self.value), 'Not a number: {}'.format(self.value)

    def to_unicode(self, ctx):
        return unicode("<{}>".format(self.value))


class __forbidden_Unicode(BaseType):
    def __init__(self, value):
        self.value = unicode(value)

    def to_unicode(self, ctx):
        return self.value
