# -*- coding: utf-8 -*-

from collections import defaultdict
from data_object import *
from statements import *
from expression import *
from functools import partial
import json
from doc_types import *

import context


def get_node(root, x_path):
    _list = list(root.xpath(x_path))
    return _list[0] if _list else None


def process_doc(doc):
    ctx = None

    return Document([process_struct(ctx, item) for item in doc.xpath('./Repeat/STRUCT')])


def process_struct(ctx, e_struct):
    struct_type = UnicodeExpr(get_node(e_struct, './Re').text)
    e_struct_id = get_node(e_struct, './/STRUCT_ID')

    if e_struct_id is None:
        struct_id = None
    else:
        struct_id = UnicodeExpr(e_struct_id.text, element=e_struct_id)

    headline = process_stext(ctx, get_node(e_struct, './STEXT'))
    e_definition = get_node(e_struct, './Repeat/Chain/DEFINITION')

    if e_definition is None:
        definition = None
    else:
        definition = process_definition(ctx, e_definition)

    paragraphs = process_struct_body(ctx, get_node(e_struct, './STRUCT_BODY'))

    return Structure(struct_type, struct_id, headline, definition, paragraphs)


def process_stext(ctx, e_stext):
    return process_stext_body(ctx, get_node(e_stext, './Choice/STEXT_BODY'))


def process_stext_body(ctx, e_stext_body):
    def process_one(item):
        return process_stext_element(ctx, item)

    return STextExpr(map(process_one, e_stext_body.xpath('./STEXT_ELEMENT')),
                     element=e_stext_body)


def process_definition(ctx, e_definition):
    mapper = partial(process_assignment, ctx)

    return Definition(map(mapper, e_definition.xpath('.//ASSIGNMENT')))


def process_assignment(ctx, e_assignment):
    name = get_node(e_assignment, './Re').text
    func_args = e_assignment.xpath('./Repeat/FUNC_DEF/DEF_ARGS//Re')
    expr = process_expr(ctx, get_node(e_assignment, './EXPR'))

    if func_args:
        expr = Function([e.text for e in func_args],
                        expr,
                        element=e_assignment)

    return Assignment(name, expr)


def process_struct_body(ctx, e_struct_body):
    def process_one(item):
        return process_stext_body(ctx, item)

    x_bodies = './Chain/STEXT_BODY | ./Chain/Repeat/Chain/STEXT_BODY'

    return map(process_one, e_struct_body.xpath(x_bodies))


def process_stext_element(ctx, e_stext_element):
    handlers = {'UNICODE': process_unicode,
                'Q': process_quote,
                'DASH': process_dash,
                'STEXT': process_stext,
                'F_CALL': process_f_call,
                'EXPR': process_expr,
                'FLAGS': process_flags}

    x_nodes = './STEXT | ./UNICODE | ./Q | ./DASH |' \
              './F_CALL | ./EVAL_EXPR/EXPR | ./FLAGS'

    inner = get_node(e_stext_element, x_nodes)

    if inner is None:
        #FIXME: add support for new tags.
        return NoneExpr()

    return handlers[inner.tag](ctx, inner)


def process_unicode(ctx, unicode_element):
    return UnicodeExpr(unicode_element.text, unicode_element)


def process_quote(ctx, e_quote):
    return QuoteExpr(e_quote.text, element=e_quote)


def process_dash(ctx, e_dash):
    return DashExpr(e_dash.text, element=e_dash)


def process_f_call(ctx, f_call_element):
    f_arg_element = get_node(f_call_element, './Choice/Chain/F_ARG')
    args = [process_f_arg(ctx, f_arg_element)] if f_arg_element is not None else []

    for re_element in reversed(f_call_element.xpath('./ID_SEQ/Re | ./ID_SEQ/Repeat/Chain/Re')):
        args = [FCallExpr(re_element.text, args, re_element)]

    return args[0]


def process_f_arg(ctx, f_arg_element):
    handlers = {'EXPR': process_expr,
                'STEXT': process_stext}

    elem = get_node(f_arg_element, './EVAL_EXPR/EXPR | ./STEXT')

    return handlers[elem.tag](ctx, elem)


def process_expr(ctx, e_expr):
    handlers = {'ADD': process_math,
                'SUB': process_math,
                'PROD': process_math,
                'NUM': process_num,
                'VAR': process_var,
                'EXPR': process_expr,
                'EXPR_F_CALL': process_expr_f_call,
                'STEXT': process_stext,
                'P1': process_expr}

    e_expr = get_node(e_expr, './ADD | ./SUB | ./PROD | ./NUM | ./VAR'
                              '| ./EXPR_GROUP/EXPR | ./EXPR_F_CALL | ./STEXT | ./P1')

    return handlers[e_expr.tag](ctx, e_expr)


def process_math(ctx, elem):
    ops = {'+': AddExpr,
           '-': SubExpr,
           '*': MultExpr,
           '/': DivExpr}

    op = get_node(elem, './String | ./MULT_OP').text
    args = elem.xpath('./EXPR | ./P1')

    return ops[op]([process_expr(ctx, e_expr) for e_expr in args],
                   element=elem)


def process_num(ctx, num_element):
    return ConstExpr(float(num_element.text))


def process_var(ctx, e_var):
    return NameExpr(e_var.text, element=e_var)


def process_expr_f_call(ctx, e_f_call):
    args = e_f_call.xpath('./EXPR_F_CALL_ARGS/Chain/EXPR | '
                          './EXPR_F_CALL_ARGS/Chain/Repeat/Chain/EXPR')
    func_name = get_node(e_f_call, './Re').text

    return FCallExpr(func_name,
                     [process_expr(ctx, e_expr) for e_expr in args],
                     element=e_f_call)


def process_flags(ctx, e_flags):
    flags = (e.text for e in e_flags.xpath('.//Re'))
    return FlagsExpr(flags, element=e_flags)
