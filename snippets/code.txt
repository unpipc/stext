article is "article"

.sub "Header" by F. L. Author
.def
    ## Типы данных.
    # Числа
    i = 34
    f = 87.223

    # Простой текст:
    s_basic = "Basic text"

    # Список свойств:
    @my_style: italic, bolder

    # Метатекст:
    s = {Bit of meta /@my_style:{text}...}

    ## Операции:

    a = 3 + 4 - 5 * 6   # Простые.
    a = 3 / 4           # Полноценное деление, == 0.75
    a = mod(3, 4)       # Остаток от деления.
    a = pow(3.1415, 2)  # Возведение в степень.

    s2 = {Second bit of text}

    # Конкатенация:
    s3 = s1 + s2
    s4 = {$(s1)$(s2)}
    s4 = fmt("%s%.2f", s1, 3.1415)
    s4 = fmt({%s/@italic:[%.2f]}, s1, 3.1415)

    b = len(s3) * 2

    f = (a, b): (a + b) * i
.end-def