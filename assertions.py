index = lambda element: int(element.attrib['at'].split('-')[0])


class SemanticError(Exception):
    etype = 'Semantic'

    def __init__(self, index, message, end_index=None, element=None):
        super(SemanticError, self).__init__(message)

        self.index = index
        self.end_index = end_index
        self.element = element


class PySemanticError(Exception):
    etype = 'Semantic'

    def __init__(self, message):
        super(PySemanticError, self).__init__(message)


def Assert(cond, element, error_msg):
    if not cond:
        SemanticError(index(element), error_msg)


KEYWORDS = {'def', 'end-def',

            'normal',
            'bold', 'bolder', 'lighter',
            'small-caps',
            'serif', 'sans-serif', 'monospace', 'fantasy', 'cursive',
            'italic', 'oblique',
            'larger', 'smaller',
            'xx-small', 'x-small', 'small', 'medium', 'large', 'x-large', 'xx-large'}


def identifier(element, i):
    Assert(i not in KEYWORDS, element, 'Bad identifier')


number = lambda n: isinstance(n, (int, float))


def string(element, s):
    Assert(isinstance(s, basestring), element, 'Not a string: {}'.format(`s`))