#!/usr/bin/env python

from chedi import *


re_ident = r'[a-zA-Z_.][a-zA-Z_.0-9]*'

l_blank = Lexem('blank', '[ \t]+', ignore=True)
l_nl = Lexem('nl', r'\n')
l_comma = Lexem('comma', r',')
l_tilde = Lexem('tilde', r'~')
l_eq = Lexem('eq', r'=')


l_chars = Lexem('chars', r'[^~\n{}/$@\\`\'"<>-]+')
l_num = Lexem('num', r'[\d]+(\.[\d]*)?')

l_slashed = Lexem('slashed', r'\\.')
l_property = Lexem('property', '@' + re_ident)
l_idents = Lexem('idents',)
l_ident_no_dot = Lexem('ident_no_dot', r'[a-zA-Z_][a-zA-Z_0-9]*')

l_q = Lexem('q', r"`+|'+|\"+|<+|>+")
l_ofb = Lexem('ofb', r'\{')
l_cfb = Lexem('cfb', r'\}')
l_ocb = Lexem('ocb', r'\(')
l_ccb = Lexem('ccb', r'\)')

l_dash = Lexem('dash', r'-+')
l_struct_start = Lexem('struct_start', r'~{}'.format(re_ident))

lexer = Lexer()
lexer.add_lexems([l_chars, l_blank, l_comma, l_nl, l_tilde, l_num, l_eq, l_slashed, l_property, l_ident_no_dot,
                  l_q, l_ofb, l_cfb, l_ocb, l_ccb, l_dash,
                  l_struct_start])

s = Sketch()

te = s.define('stext_element', {l_chars, l_q, l_dash, l_slashed, nt('stext')})
stext = s.define('stext', [l_ofb, {nt('stext'), nt('stext_body')}, l_cfb])

stext_body = s.define('stext_body', [te, nt('stext_body')], or_lambda=True)

struct_body = s.define('struct_body', (stext_body, nt('struct_body_')), or_lambda=True)
struct_body_ = s.define('struct_body_', (l_nl, nt('struct_body_')), or_lambda=True)

struct = s.define('struct', [l_struct_start, nt('struct_id'), stext, l_nl, struct_body])
struct_id = s.define('struct_id', [l_eq, l_ident])

doc = s.define('doc', [struct, nt('doc')], or_lambda=True, target=True)

with open('chedi_samples/text1.stxt') as in_file:
    in_text = in_file.read().decode('utf-8')

    print in_text
    print '\n'.join(map(lambda token: token.name,
                        lexer.analyze(in_text)))