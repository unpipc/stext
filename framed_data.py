#!/usr/bin/env python
import helpers
import unittest
import collections
import operator


class FramedData(object):
    def __init__(self):
        self.storage = collections.defaultdict(list)
        self.keys_set_in_frame = collections.defaultdict(set)
        self.frame_no = None

    def open(self):
        self.frame_no = 0 if self.frame_no is None else self.frame_no + 1

    def close(self):
        for key in self.keys_set_in_frame[self.frame_no]:
            self.storage[key].pop()

        del self.keys_set_in_frame[self.frame_no]
        self.frame_no = None if self.frame_no == 0 else self.frame_no - 1

    def __getitem__(self, path):
        assert isinstance(path, basestring)

        keys = path.split('.')
        key0 = keys[0]
        objects = self.storage[key0]

        if objects:
            obj = objects[-1]
        else:
            raise KeyError(key0)

        return helpers.deep_get(obj, keys[1:])

    def __setitem__(self, path, value):
        assert isinstance(path, basestring)

        if self.frame_no is None:
            raise Exception('Frame needs opening before setting a value')

        keys = path.split('.')
        parent_path = keys[:-1]
        key = keys[-1]

        if parent_path:
            obj = self['.'.join(parent_path)]
            obj[key] = value
        else:
            objects = self.storage[key]

            if key in self.keys_set_in_frame[self.frame_no]:
                # This key has been set in this frame.
                # Use the same slot.
                objects[-1] = value
            else:
                objects.append(value)
                self.keys_set_in_frame[self.frame_no] |= {key}


class Test(unittest.TestCase):
    def test_closed_frame(self):
        do = FramedData()
        do.open()
        do['a'] = 'A'
        do.close()
        self.assertRaises(KeyError, operator.getitem, do, 'a')

    def test_overriding(self):
        do = FramedData()
        do.open()
        do['a'] = 'A'
        do.open()
        do['a'] = 'AA'
        self.assertEqual(do['a'], 'AA')
        do.close()
        self.assertEqual(do['a'], 'A')
        do.close()
        self.assertRaises(Exception, operator.getitem, do, 'a')

    def test_all(self):
        do = FramedData()
        do.open()
        do['a'] = dict()
        do['a.b'] = 'B'
        do['a.c'] = 'C'

        self.assertEqual(do['a.b'], 'B')
        self.assertEqual(do['a.c'], 'C')
        self.assertRaises(KeyError, operator.getitem, do, 'a.b.d')

if __name__ == '__main__':
    unittest.main()