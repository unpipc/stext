import abc
import json

from framed_data import FramedData
from context import Context, global_objects
from assertions import *
from expression import BaseExpr, NoneExpr


class DocType(object):
    __metaclass__ = abc.ABCMeta

    def to_dict(self, ctx):
        pass

    def to_json(self, ctx):
        return json.dumps(self.to_dict(ctx), cls=ExprEncoder)


class ExprEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, BaseExpr):
            return o.value
        else:
            return super(ExprEncoder, self).default(o)


class Document(DocType):
    def __init__(self, structs):
        self.structs = structs

    def to_dict(self, ctx):
        structs = []
        for s in self.structs:
            obj = s.to_dict(ctx)
            structs.append(obj)

        return {'structs': structs}


class Structure(DocType):
    def __init__(self, struct_type, struct_id, headline, definition, paragraphs):
        self.struct_type = struct_type
        self.struct_id = struct_id or NoneExpr()
        self.headline = headline
        self.definition = definition
        self.paragraphs = paragraphs

    def to_dict(self, ctx):
        ctx.open()
        lctx = ctx.local_objects
        lctx['this'] = dict()

        id_value = self.struct_id.value

        if id_value is not None:
            identifier(self.struct_id.element, id_value)
            lctx['this.id'] = self.struct_id

            try:
                ctx.global_objects[id_value] = ctx['this']
            except KeyError as e:
                raise SemanticError(index(self.struct_id.element), e.message)

        headline = self.headline.calculate(ctx)
        lctx['this.headline'] = headline

        if self.definition is not None:
            self.definition.execute(ctx)

        paragraphs = map(lambda obj: obj.calculate(ctx),
                         self.paragraphs)

        ctx.close()

        return {'id': self.struct_id,
                'type': self.struct_type,
                'headline': headline,
                'paragraphs': paragraphs}