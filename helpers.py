def deep_get(obj, keys):
    for key in keys:
        if isinstance(obj, dict):
            obj = obj[key]
        else:
            raise KeyError(key)

    return obj
