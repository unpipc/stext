#!/usr/bin/env python

from cfg import *
from article import *

from semantics import process_doc, SemanticError
from context import Context, global_objects
import localization

re_unicode = '[^~\n{}/$@\\\\`\'"<>-]*'
re_slashed = '\\\\.'
re_id = '[a-zA-Z_.][a-zA-Z_.0-9]*'
re_id_no_dot = '[a-zA-Z_][a-zA-Z_0-9]*'
re_blank = '[ \t]*'
re_blank_sep = '[ \t]+'

T = BNF(auto_choice=False)

_ = T.define('BLANK', RE(re_blank))
_nl = T.define('_NL', RE('{blank}\n'.format(blank=re_blank)))
nl = T.define('NL', '\n')
_nl_ = T.define('_NL_', RE('{blank}\n{blank}'.format(blank=re_blank)))
__ = T.define('BLANK_SEPARATOR', RE(re_blank_sep))
q = T.define('Q', RE("`+|'+|\"+|<+|>+"))
dash = T.define('DASH', RE("-+"))
id_seq = T.define("ID_SEQ", [_, RE(re_id), ANY([_, ',', _, RE(re_id)])])

num = T.define('NUM', RE('[\d]+(\.[\d]*)?'))
var = T.define('VAR', RE(re_id))
mult_op = T.define('MULT_OP', RE('[/*]'))

expr_f_call_args = T.define('EXPR_F_CALL_ARGS', OPT([SYM('EXPR'), ANY([',', _, SYM('EXPR')])]))
expr_f_call = T.define('EXPR_F_CALL', [RE(re_id), _, '(', _, expr_f_call_args, _, ')'])

expr_group = T.define('EXPR_GROUP', ['(', _, SYM('EXPR'), _, ')'])
p1_expr = T.define('P1', CHOICE(SYM('PROD'), expr_group, num, var, expr_f_call))

prod = T.define('PROD', [p1_expr, _, mult_op, _, p1_expr])
add = T.define('ADD', [SYM('EXPR'), _, '+', _, SYM('EXPR')])
sub = T.define('SUB', [SYM('EXPR'), _, '-', _, p1_expr])

expr = T.define('EXPR', expr_group)
T.define('EXPR', expr_f_call)
T.define('EXPR', add)
T.define('EXPR', sub)
T.define('EXPR', prod)
T.define('EXPR', num)
T.define('EXPR', var)
T.define('EXPR', SYM('STEXT'))

eval_expr = T.define('EVAL_EXPR', ['$(', expr, ')'])

flags = T.define('FLAGS', ['@(', id_seq, ')'])

f_arg = T.define('F_ARG', SYM('STEXT'))
T.define('F_ARG', eval_expr)

f_call = T.define('F_CALL',
                  ['/',
                   id_seq,
                   CHOICE('/', [':', f_arg]),
                  ])

u = T.define('UNICODE', RE(re_unicode))
slashed = T.define('SLASHED', RE(re_slashed))

te = T.define('STEXT_ELEMENT', u)
T.define('STEXT_ELEMENT', q)
T.define('STEXT_ELEMENT', dash)
T.define('STEXT_ELEMENT', slashed)
T.define('STEXT_ELEMENT', f_call)
T.define('STEXT_ELEMENT', eval_expr)
T.define('STEXT_ELEMENT', flags)
T.define('STEXT_ELEMENT', SYM('STEXT'))

stext = T.define('STEXT', ['{',
                           CHOICE(SYM('STEXT'), SYM('STEXT_BODY')),
                           '}'])

stext_body = T.define('STEXT_BODY', ANY(te))

struct_body = T.define('STRUCT_BODY', [stext_body, ANY([nl, stext_body])])
T.define('STRUCT_BODY', stext_body)
T.define('STRUCT_BODY', LAMBDA)

def_args = T.define("DEF_ARGS", [RE(re_id_no_dot), ANY([_, ',', _, RE(re_id_no_dot)])])
func_def = T.define('FUNC_DEF', ['(', _, def_args, _, ')', _, ':'])
assignment = T.define('ASSIGNMENT', [RE(re_id), _, '=', _, OPT(func_def), _, expr])

one_line_def = T.define('ONE_LINE_DEF', ['~def:', __, assignment])

statements = T.define('STATEMENTS', [_, OPT(assignment), ANY([_, ';', _, OPT(assignment)])])
statements_group = T.define('STATEMENTS_GROUP', [statements, _nl_, ANY([statements, _nl_])])

def_block = T.define('DEF_BLOCK', ['~def', statements_group, '~end-def'])
definition = T.define('DEFINITION', CHOICE(one_line_def, def_block))

struct_id = T.define('STRUCT_ID', RE(re_id))
struct = T.define('STRUCT', ['~', RE(re_id), OPT(['=', struct_id]), __, stext, _nl,
                             OPT([definition, _nl]), struct_body])

doc = T.define('DOC', ANY(struct), target=True)
T.define('DOC', LAMBDA)

with open('samples/text.stxt') as in_file:
    structured_text = in_file.read().decode('utf-8')

    try:
        #root = T.parse(structured_text)
        #root.xml_tree().write(sys.stdout)
        document = process_doc(T.parse(structured_text).xml_tree().getroot())

        context = Context(global_objects,
                          locale=localization.ru,
                          convert_to='html')
        doc_json = document.to_json(context)
        print Article(doc_json).to_html().encode('utf-8')
    except (ParsingError, SemanticError) as e:
        einfo = error_info(e,
                           structured_text,
                           surrounding_patterns=('...', '...'),
                           surrounding_chars=(32, 8))

        if einfo['eof_error']:
            print 'Unexpected end of file.'
        else:
            print einfo['error_line_info']['formatted_w_pointer']
