# -*- coding: utf-8 *-*

import abc
from stypes import *
from assertions import index


def raise_semantic_error(element, err_msg):
    if element:
        raise SemanticError(index(element), err_msg)
    else:
        raise PySemanticError(err_msg)


class BaseExpr(object):
    __metaclass__ = abc.ABCMeta
    expr_name = 'Base Expression'

    def __init__(self, element=None):
        self.element = element

    def calculate(self, ctx):
        # To be overridden in expression subclasses.
        raise_semantic_error(self.element, "Not calculatable")

    def __call__(self, ctx, *args):
        # To be overridden in function subclasses.
        raise_semantic_error(self.element, "Not a function: {}".format(self.expr_name))

    def to_unicode(self, ctx):
        return unicode(self.calculate(ctx).value)


class SimpleExpr(BaseExpr):
    expr_name = 'Simple Expression'

    def __init__(self, value, element=None):
        super(SimpleExpr, self).__init__(element)
        self.value = value

    def calculate(self, ctx):
        return self


class UnicodeExpr(SimpleExpr):
    expr_name = "Unicode Text Expression"

    def __init__(self, value, element=None):
        super(UnicodeExpr, self).__init__(unicode(value), element)


class QuoteExpr(BaseExpr):
    expr_name = "Quote"

    def __init__(self, text_pattern, element=None):
        super(QuoteExpr, self).__init__(element)

        self.text_pattern = text_pattern

    def calculate(self, ctx):
        try:
            return UnicodeExpr(ctx.local_settings['locale'].quotes_map[self.text_pattern])
        except KeyError:
            raise SemanticError(index(self.element),
                                "Wrong quote pattern in this locale",
                                element=self.element)


class DashExpr(BaseExpr):
    expr_name = "Dash"

    def __init__(self, text_pattern, element=None):
        super(DashExpr, self).__init__(element)

        self.text_pattern = text_pattern

    def calculate(self, ctx):
        dashes = {'-': u'‐', #\u2010
                  '--': u'–', # \u2013
                  '---': u'—'} # \u2014
        try:
            return UnicodeExpr(dashes[self.text_pattern])
        except KeyError:
            raise SemanticError(index(self.element),
                                "Wrong dash pattern",
                                element=self.element)


class ConstExpr(SimpleExpr):
    expr_name = 'Constant'

    def __init__(self, value, element=None):
        super(ConstExpr, self).__init__(float(value), element)


class NameExpr(BaseExpr):
    expr_name = 'Constant Reference'

    def __init__(self, name, element=None):
        super(NameExpr, self).__init__(element)
        self.reference = name

    def calculate(self, ctx):
        try:
            value = ctx[self.reference]
        except KeyError as e:
            raise SemanticError(index(self.element), "Undefined reference: {}".format(e))

        return value.calculate(ctx)


class AddExpr(BaseExpr):
    expr_name = 'Addition'

    def __init__(self, operands, element=None):
        super(AddExpr, self).__init__(element)
        self.operands = operands

    def calculate(self, ctx):
        return ConstExpr(sum(expr.calculate(ctx).value for expr in self.operands))


class SubExpr(BaseExpr):
    expr_name = 'Subtraction'

    def __init__(self, expressions, element=None):
        super(SubExpr, self).__init__(element)
        self.operands = expressions

    def calculate(self, ctx):
        a, b = [expr.calculate(ctx).value for expr in self.operands]

        return ConstExpr(a - b)


class MultExpr(BaseExpr):
    expr_name = 'Multiplication'

    def __init__(self, operands, element=None):
        super(MultExpr, self).__init__(element)
        self.operands = operands

    def calculate(self, ctx):
        a, b = [expr.calculate(ctx).value for expr in self.operands]

        return ConstExpr(a * b)


class DivExpr(BaseExpr):
    expr_name = 'Division'

    def __init__(self, expressions, element=None):
        super(DivExpr, self).__init__(element)
        self.operands = expressions

    def calculate(self, ctx):
        a, b = [expr.calculate(ctx).value for expr in self.operands]

        return ConstExpr(a / b)


class Function(BaseExpr):
    expr_name = 'Function'

    def __init__(self, args_names, body_expr, element=None):
        super(Function, self).__init__(element)
        self.args_names = args_names
        self.body_expr = body_expr

    def __call__(self, ctx, *args):
        named_args = zip(self.args_names, args)

        if len(self.args_names) != len(named_args):
            SemanticError(index(self.element), "Wrong arguments")

        lctx = ctx.local_objects
        lctx.open()

        for name, expr in named_args:
            lctx[name] = expr

        result = self.body_expr.calculate(ctx)

        lctx.close()

        return result


class FCallExpr(BaseExpr):
    expr_name = 'Function Call'

    def __init__(self, name, arguments, element=None):
        super(FCallExpr, self).__init__(element)
        self.reference = name
        self.arguments = arguments

    def calculate(self, ctx):
        try:
            expr = ctx[self.reference]
        except KeyError as e:
            raise SemanticError(index(self.element), "Undefined reference: {}".format(e))

        if not isinstance(expr, Function):
            raise SemanticError(index(self.element), "Not a function")

        args = [e.calculate(ctx) for e in self.arguments]
        return expr(ctx, *args)


class STextExpr(BaseExpr):
    expr_name = 'Structured Text'

    def __init__(self, inner_elements, element=None):
        super(STextExpr, self).__init__(element)
        self.inner_elements = inner_elements

    def calculate(self, ctx):
        return UnicodeExpr(self.to_unicode(ctx))

    def to_unicode(self, ctx):
        parts = (item.to_unicode(ctx) for item in self.inner_elements)

        return u"".join(parts)


class FlagsExpr(BaseExpr):
    def __init__(self, flags, element=None):
        super(FlagsExpr, self).__init__(element)
        self.flags = flags

    def calculate(self, ctx):
        try:
            for flag in self.flags:
                ctx.global_objects[flag].calculate(ctx)
        except KeyError as e:
            raise_semantic_error(self.element, "Unknown flag: {}".format(e))

        return VoidExpr()


class NoneExpr(SimpleExpr):
    expr_name = 'None Expression'

    def __init__(self):
        super(NoneExpr, self).__init__(None)

    def calculate(self, ctx):
        return self

    def to_unicode(self, ctx):
        return unicode('')


class VoidExpr(SimpleExpr):
    expr_name = 'Void Expression'

    def __init__(self):
        super(VoidExpr, self).__init__('')

    def calculate(self, ctx):
        return self
